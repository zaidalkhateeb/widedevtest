<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('Auth')->group(function (){
    Route::post('register',[\App\Modules\AuthModule\Controllers\AuthController::class,'register']);
    Route::post('login',[\App\Modules\AuthModule\Controllers\AuthController::class,'login']);
});

Route::middleware('auth:api')->group( function () {
    Route::prefix('Orders')->group(function (){
        Route::post('/',[\App\Modules\OrderModule\Controllers\OrderController::class,'createOrder']);
        Route::get('/{order}',[\App\Modules\OrderModule\Controllers\OrderController::class,'getOrderById']);
        Route::get('/',[\App\Modules\OrderModule\Controllers\OrderController::class,'getOrdersByUser']);
        Route::put('/{order}',[\App\Modules\OrderModule\Controllers\OrderController::class,'updateOrder']);
    });

    // Coming Soon
    Route::prefix('Payments')->group(function (){
        Route::get('/order/{order}',[\App\Modules\OrderModule\Controllers\OrderController::class,'orderPayments']);
        Route::post('{payment}/continue',[\App\Modules\OrderModule\Controllers\OrderController::class,'continueBill']);
        Route::put('{payment}',[\App\Modules\OrderModule\Controllers\OrderController::class,'updatePayment']);
    });

    // Coming Soon
    Route::prefix('products')->group(function (){

    });


});


