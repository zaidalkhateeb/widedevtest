<?php


namespace App\SharedServices;


class ResponseService
{
    public static function sendResponse($data,$message,$status){
        return response()->json([
            'data' => $data,
            'message' => $message,
        ], $status);
    }
}
