<?php


namespace App\Modules\OrderModule\Services;


use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Payment;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use League\Flysystem\Config;

class OrderService
{
    public static function createOrder($orderProduct, $user)
    {

        DB::beginTransaction();
        $orderProduct['user_id'] = $user->id;
        $orderProduct['order_status'] = \config('enums.orderStatus.Pending');
        $order = Order::create($orderProduct);

        $products = Product::selectRaw('id as product_id,' . $order->id . ' as order_id,name as product_name,price as product_price')
            ->whereIn('id', $orderProduct['products'])
            ->get();
        OrderProduct::insert($products->toArray());
        if (isset($orderProduct['payment_type'])) {
            // if user pay directly so the order is approved
            $payment = [
                'order_id' => $order->id,
                'amount' => $orderProduct['amount'],
                'payment_type' => $orderProduct['payment_type']
            ];
            $amount = isset($orderProduct['discount_percentage']) ?
                $orderProduct['amount'] - $orderProduct['discount_percentage'] * $orderProduct['amount'] :
                $orderProduct['amount'];
            if ($amount > $products->sum('product_price')) {
                abort(\Illuminate\Http\Response::HTTP_BAD_REQUEST, 'the amount is bigger than bill total');
            }
            Payment::create($payment);
            $order->order_status = \config('enums.orderStatus.Accept');
            $order->save();
        }
        DB::commit();

    }

    public static function getOrderById($order)
    {
        $data = Order::with(['products:order_id,id,product_name,product_price',
            'user:id,name,email,surname',
            'payments:id,order_id,amount'])
            ->withSum('products', 'product_price')
            ->withSum('payments', 'amount')
            ->where('id', $order->id)
            ->get();
        return $data;
    }


    public static function getOrdersByUser($user_id)
    {
        $data = Order::with(['products:order_id,id,product_name,product_price',
            'payments:id,order_id,amount'])
            ->withSum('products', 'product_price')
            ->withSum('payments', 'amount')
            ->where('user_id', $user_id)
            ->get();
        return $data;
    }

    public static function updateOrder($order,$product_id)
    {
        // Check if Product Exist
        $product=Product::findOrFail($product_id);

        // Add New Product
        $data=OrderProduct::insert([
            'order_id'=>$order->id,
            'product_id'=>$product_id,
            'product_name'=>$product->name,
            'product_price'=>$product->price
        ]);
        return $data;

    }
}
