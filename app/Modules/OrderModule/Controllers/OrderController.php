<?php


namespace App\Modules\OrderModule\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Modules\OrderModule\Requests\CreateOrderRequest;
use App\Modules\OrderModule\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function createOrder(CreateOrderRequest $order)
    {
        $user = Auth::user();
        $order = $order->validated();
        OrderService::createOrder($order, $user);
        return response()->json(['data' => null, 'message' => 'success'], Response::HTTP_OK);
    }

    public function getOrderById(Order $order, Request $request)
    {
        $data = OrderService::getOrderById($order);
        return response()->json(['data' => $data, 'message' => 'success'], Response::HTTP_OK);
    }

    public function getOrdersByUser(Request $request)
    {
        $request->validate(['user_id'=>'required']);
        $data = OrderService::getOrdersByUser($request->user_id);
        return response()->json(['data' => $data, 'message' => 'success'], Response::HTTP_OK);
    }


    public function updateOrder(Order $order,Request $request)
    {

        $order=OrderService::updateOrder($order,$request->product_id);
        return response()->json(['data' => $order, 'message' => 'success'], Response::HTTP_OK);
    }

}
