<?php

namespace App\Modules\AuthModule\Controllers;

use App\AuthModule\Requests\LoginRequest;
use App\AuthModule\Requests\RegisterUserRequest;
use App\AuthModule\Services\AuthService;
use App\SharedServices\ResponseService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Client\Response;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{

    var $message = 'success';

    public function register(RegisterUserRequest $user)
    {
        $date = AuthService::Register($user->validated());
        return ResponseService::sendResponse($date, $this->message, \Illuminate\Http\Response::HTTP_OK);
    }

    public function login(LoginRequest $user){

        $date = AuthService::Login($user);
        if(!$date){
            return  ResponseService::sendResponse(null,'Wrong Password Or Username',\Illuminate\Http\Response::HTTP_FORBIDDEN);
        }
        return ResponseService::sendResponse($date, $this->message, \Illuminate\Http\Response::HTTP_OK);
    }
}
