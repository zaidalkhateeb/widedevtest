<?php


namespace App\Modules\AuthModule\Services;


use App\Models\User;
use App\SharedServices\ResponseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{

    public static function Register($user)
    {
        // hash password
        $user['password'] = Hash::make($user['password']);
        $user = User::create($user);

        $accessToken = $user->createToken('token')->accessToken;
        $user['access_token'] = $accessToken;
        return $user;
    }

    public static function Login($user)
    {

        if (!auth()->attempt(['email' => $user->email, 'password' => $user->password])) {
            return false;
        }
        $user = Auth::user();
        $accessToken = $user->createToken('token')->accessToken;
        $user['access_token'] = $accessToken;
        return $user;
    }
}
