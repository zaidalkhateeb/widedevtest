<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static selectRaw(string $string)
 * @method static findOrFail($product_id)
 */
class Product extends Model
{
    use HasFactory;
    protected $guarded=[];


    public function product(){
        return $this->hasMany(OrderProduct::class);
    }
}
