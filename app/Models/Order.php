<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create($orderProduct)
 */
class Order extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'discount_note',
        'discount_percentage',
        'order_status',
        'order_date'
    ];




    public function setOrderStatusAttribute($value)
    {
        $this->attributes['order_status']=$value;
    }


    public function getOrderStatusAttribute($value)
    {
        foreach (config('enums.orderStatus') as $index=>$elm){
            if($value ==$elm)
                return $index ;
        }
    }



    public function user(){
        return $this->belongsTo(User::class);
    }

    public function products(){
        return $this->hasMany(OrderProduct::class);
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }
}
