<?php
return
[
    'orderStatus'=>[
        'Pending'=>0,
        'Accept'=>1,
        'Decline'=>2,
        'Cancel'=>3
    ],
    'paymentType'=>[
        'Cash'=>1,
        'Debt'=>2,
        'CreditCard'=>3,
        'Wirettransfer'=>4,
        'Free'=>5
    ]

];
